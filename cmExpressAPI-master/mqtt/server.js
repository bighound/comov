// MQTT Server
var aedes = require('aedes')();
var mqttServer = require('net').createServer(aedes.handle);
var mqttPort = 1883;

var ChatController = require('../controllers/chat');

mqttServer.listen(mqttPort, function () {
  console.log('Servidor MQTT escuchando en http://localhost:' + mqttPort);
})

aedes.on('subscribe', function (subscriptions, client) {
    console.log('Cliente ' + client.id + " suscrito a " + subscriptions.map(s => s.topic).join('\n'));
})

// Cuando un usuario se conecta
aedes.on('client', function (client) {
    console.log('Cliente MQTT conectado: ' + client.id);
})

// Cuando un mensaje se publica
aedes.on('publish', async function (packet, client) {
    if (client) {   
        var message = JSON.parse(packet.payload.toString());
        if (message.type == 'message' && packet.topic == 'messages_channel') {
            // Obtener el chat
            var values = {
                id: message.chatId,
                message: {'uid': message.idCli, 'body': message.text, 'date': message.date},
                date: message.date
            }

            ChatController.updateChatMQTT(values);
        
            // Publicar en la cola de cada miembro del chat
            message.members.forEach(member => {
                if (member._id != message.idCli)
                    aedes.publish({
                        cmd: 'publish',
                        qos: 2,
                        topic: member._id,
                        payload: new Buffer.from(JSON.stringify(message)),
                        retain: false
                    });
            });

            console.log('Message published MQTT');
        }
    }
})

// Avisar a los miembros del chat/grupo
exports.announNewChat = function (payload) {
        var message = JSON.parse(payload);

        // Publicar la creacion de un chat nuevo
        if ((message.type == 'chat' || message.type == 'group') && message.topic == 'chats_channel') {
            console.log('Sending notifications...');
            message.members.forEach(member => {
                if (member._id != message.idCli) {
                    aedes.publish({
                        cmd: 'publish',
                        qos: 2,
                        topic: member._id,
                        payload: new Buffer.from(payload),
                        retain: false
                    });
                }
            });

            console.log('Chat created MQTT');
        }
}