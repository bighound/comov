'use strict'
var mongoose = require('mongoose');

// Cargamos los modelos para usarlos posteriormente
var Chat = require('../models/chat');
var User = require('../models/user');

var UserController = require('../controllers/user');
var serverMQTT = require('../mqtt/server')

// Obtener un chat
exports.getChat = function (req, res) {
    Chat.findById(req.params.id).exec(function (err, chatObj) {
        // Si la peticion tiene algun error...
        if (err) {
            console.log(err);
            res.status(500).send({
                error: 'Request error'
            })
        } else if (!chatObj) { // Si no encontramos el objeto
            res.status(404).send({
                error: 'Not found'
            })
        } else { // Podemos modificar el objeto antes de devolverlo, por ejemplo, quitando elementos.
            return res.status(200).send({
                chatObj
            });
        }
    });
}

// Crear un chat
exports.postChat = async function (req, res) {
    if (req.body._id == 'create') 
        req.body._id = mongoose.Types.ObjectId();
    
    var chatComp = {
        chatName: req.body.cname,
        friendName: '',
        username: '',
    }
    
    // Modificar el nombre del chat 
    if (req.body.typeChat === 'chat') {
        chatComp.chatName = '';
        req.body.members.forEach(memberL => {
            if (memberL._id !== req.body.creatorId) {
                chatComp.friendName = memberL.name
                chatComp.chatName += memberL.name + '$'
            } else {
                chatComp.username = memberL.name
                chatComp.chatName += memberL.name + '$'
            }
        }); 
        
        req.body.cname = chatComp.chatName;
        console.log("Chat privado:", chatComp);
    }

    // Fechas de creación y última actualización
    if (req.body.createdAt === undefined)
        req.body.createdAt = new Date().toISOString();
    if (req.body.updatedAt === undefined)
        req.body.updatedAt = new Date().toISOString();

    Chat.create(req.body, function (err, newChat) {
        if (err) {
            return res.status(400).send({
                body: req.body,
                error: 'Bad request'
            });
        }

        var chatName = newChat.cname;
        var caux = newChat.cname;
        req.body.members.forEach(memberL => {
            if (req.body.typeChat === 'chat' && memberL._id !== req.body.creatorId) {
                chatName = chatComp.username;
                caux = chatComp.username;
            }
            if (req.body.typeChat === 'chat' && memberL._id === req.body.creatorId) {
                chatName = chatComp.friendName;
            }

            User.findByIdAndUpdate(memberL._id, {$push: {
                chats:{
                    _id:newChat._id, 
                    cname:chatName, 
                    createdAt:req.body.createdAt, 
                    updatedAt:req.body.updatedAt, 
                    typeChat:req.body.typeChat,
                    members:req.body.members
                }}}, {useFindAndModify:false}, function (err, response) {
                if (err) {
                    console.error(err);
                    return res.status(400).send({
                        body: req.body,
                        error: 'Bad request'
                    });
                }
            })
        });
        
        if (req.body.typeChat === 'chat')
            newChat.cname =  chatComp.friendName;
        newChat.messages = undefined;
        newChat.__v = undefined;
        // Generar los avisos mqtt
        var cpNChat = {
            chatId:newChat._id,
            chatName:caux,
            idCli:req.body.creatorId,
            type:req.body.typeChat,
            members:newChat.members,
            date:newChat.createdAt,
            topic:'chats_channel'
        };

        serverMQTT.announNewChat(JSON.stringify(cpNChat));

        return res.status(201).send({
            newChat
        });
    })
}

// Actualizar un chat, ie: añadir un mensaje, añadir un miembro
exports.updateChat = function (req, res) {
    var chat_id = req.params.id;
    var message = req.body.message;
    var member = req.body.member;
    var newDate = req.body.date;

    if ((chat_id == undefined) && (message == undefined || member == undefined) && newDate == undefined) {
        console.error('UpdateChat Error');
        return res.status(400).send({
            error: 'Request error'
        })
    }

    // Valor a insertar en la BBDD, mensaje o usuario
    const fieldToUpdate = (message != undefined) ? {messages:message} : {members:member};
    const updatedDate = {updatedAt: new Date(newDate)};

    // Falta comprobar si el usuario está en el chat o no
    Chat.findByIdAndUpdate(chat_id, {$set: updatedDate, $push: fieldToUpdate}, {useFindAndModify:false}, function (err, response) {
        if (err) {
            console.error(err);
            return res.status(500).send({
                body: req.body,
                error: 'Server error'
            });
        }
        
        return res.status(201).send({
            'status':'Chat updated'
        });
    })
}

// Actualizar un chat, ie: añadir un mensaje, añadir un miembro
exports.updateChatMQTT = function (req) {
    var chat_id = req.id;
    var message = req.message;
    var member = req.member;
    var newDate = (req.date == undefined ? new Date().toISOString() : new Date(req.date));

    if ((chat_id == undefined) && (message == undefined || member == undefined)) {
        console.error('UpdateChatMQTT Error');
        return {
            error: 'Bad request'
        };
    }

    // Valor a insertar en la BBDD, mensaje o usuario
    const fieldToUpdate = (message != undefined) ? {messages:message} : {members:member};
    const dateUpdated = {updatedAt:newDate};
    // Falta comprobar si el usuario está en el chat o no
    Chat.findByIdAndUpdate(chat_id, {$set:dateUpdated, $push: fieldToUpdate}, {useFindAndModify:false}, function (err, response) {
        if (err) {
            console.error(err);
            return {
                body: req,
                error: 'Server error'
            };
        }

        response.members.forEach(member => {
            var req = {
                uid:member._id,
                cid:chat_id,
                newdate:newDate                
            }

            UserController.updateDate(req);
        });

        return {
            'status':'Chat updated'
        };
    })
}