'use strict';

var jwt = require('jsonwebtoken')
var User = require('../models/user');
// Cargamos la librería para encriptar la contraseña
var bcrypt = require("bcrypt");
const path = require("path");

exports.postLogin = function (req, res) {
    var username = req.body.name
    var passwordHash = req.body.pHash;
    
    if (username != undefined && passwordHash != undefined) {
        User.find({
            name: username,
            //pHash: passwordHash
        }).exec(async function (err, userObj) {
            // Si la peticion tiene algun error...
            if (err) {
                console.log(err);
                res.status(500).send({
                    error: 'Request error'
                })
            } else if (!userObj || userObj === [] || userObj.length === 0) { // Si no encontramos el objeto
                res.status(404).send({
                    error: 'Not found'
                })
            } else if (! await bcrypt.compare(passwordHash, userObj[0].pHash)) { // Si la password no coincide
                res.status(404).send({
                    error: 'Not found'
                })
            }else { // Creamos el token
                var tokenData = {
                    username: username
                    // Podemos incluir fecha u otros datos relevantes
                }
                var token = jwt.sign(tokenData, 'PasswordCOMOV', {
                    expiresIn: 3600 // Duracion del token 1h
                 })
                userObj[0].pHash = undefined;
                userObj[0].__v = undefined;
                // Ordenar los chats por última actualización
                userObj[0].chats.sort(function(a, b) {
                    return new Date(b.updatedAt) - new Date(a.updatedAt);
                });
                return res.status(200).send({
                    token,
                    user: userObj[0]
                });
            }
        })
    } else {
        res.status(500).send({
            error: 'Request error'
        })
    }

}