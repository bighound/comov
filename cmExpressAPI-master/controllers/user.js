'use strict'

// Cargamos los modelos para usarlos posteriormente
var User = require('../models/user');

// Cargamos la librería para encriptar la contraseña
var bcrypt = require("bcrypt");

var BCRYPT_SALT_ROUNDS = 12;

// Obtener todos los usuarios
exports.getUsers = function (req, res) {
    User.find({}).sort('name').select(['_id', 'name', 'image']).exec(function (err, users) {
        // Si la peticion tiene algun error...
        if (err) {
            console.log(err);
            res.status(400).send({
                error: 'Request error'
            })
        } else if (!users) { // Si no encontramos el objeto
            res.status(404).send({
                error: 'Not found'
            })
        } else {
            return res.status(200).send({
                users
            });
        }
    });
}

// Buscar un usuario por su nombre y luego registra al usuario si no existe
exports.registerUser = function (req, res) {
    var uName = req.body.name;
    var uMail = req.body.email;
    var uHash = req.body.pHash;
    if (!(uName && uMail && uHash) || uName.length === 0 || uMail.length === 0 || uHash.length === 0) {
        return res.status(400).send({
            error: 'Missing fields'
        })
    }
    User.find({"name":uName}).exec(async function(err, userObj){
        // Si la peticion tiene algun error...
        if (err) {
            console.log(err);
            res.status(400).send({
                error: 'Request error'
            })
        } else if (!userObj[0]) { // Si no encontramos el usuario, lo creamos
                req.body.pHash = await bcrypt.hash(req.body.pHash, BCRYPT_SALT_ROUNDS);
                User.create(req.body, function(err, newUser) {
                    if(err) {
                        return res.status(500).send({
                            body: req.body,
                            error: 'Server error'
                        });
                    } else {
                        res.status(201).send({
                            newUser
                        })
                    }
                })
        } else {
            return res.status(409).send({
                error: 'User already exists'
            });
        }
    });
}

// Actualizar un usuario, ie: añadir un amigo
exports.updateUser = function (req, res) {
    var user_id = req.params.id;
    const friend = {
        _id:req.body._id,
        name:req.body.name
    }

    if ((user_id == undefined) || (req.body._id == undefined)) {
        console.error('UpdateUser Error');
        return res.status(400).send({
            error: 'Request error'
        })
    }
    
    const fieldToUpdate = {friends:friend};

    // Añadir el amigo si no se encuentra en la lista
    var conditions = {
        _id: user_id,
        'friends._id': { $ne: friend._id }
    };

    User.findOneAndUpdate(conditions, {$push: fieldToUpdate}, {useFindAndModify:false}, function (err, response) {
        if (err) {
            console.error(err);
            return res.status(500).send({
                body: req.body,
                error: 'Server error'
            });
        }
        
        return res.status(201).send('User updated');
    })
}

// Actualizar la fecha de un chat de usuario
exports.updateDate = function (req) {
    User.findOneAndUpdate({'_id':req.uid, 'chats._id':req.cid}, {$set:{'chats.$.updatedAt':req.newdate}}, {useFindAndModify:false}, function (err, res) {
        if (err) {
            console.error(err);
            return;
        }
    })
}