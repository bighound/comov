"use strict"; 

// Cargamos el módulo de mongoose
var mongoose = require("mongoose"); 
// Usaremos los esquemas
var Schema = mongoose.Schema;

// Importar los esquemas que utiliza Chat
var Message = require('../models/message');

// Creamos el objeto del esquema y sus atributos
var ChatSchema = Schema({
  cname: String,
  typeChat: String,
  members: [{}],
  createdAt: Date,
  updatedAt: Date,
  messages: [Message.schema],
}); 

// Exportamos el modelo para usarlo en otros ficheros
module.exports = mongoose.model("Chat", ChatSchema);