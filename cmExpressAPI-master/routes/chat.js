'use strict'

// Cargamos el módulo de express para poder crear rutas
var express = require('express');

// Cargamos el controlador
var ChatController = require('../controllers/chat');

// Llamamos al router
var api = express.Router();

// Llamamos al middleware para proteger la ruta
var mdl_auth = require('../middlewares/auth');

// Creamos una ruta para los métodos que tenemos en nuestros controladores
api.post('/chat', mdl_auth.ensureAuth, ChatController.postChat);
api.get('/chat/:id', mdl_auth.ensureAuth, ChatController.getChat);
api.post('/chat/:id', mdl_auth.ensureAuth, ChatController.updateChat);

// Exportamos la configuración
module.exports = api;